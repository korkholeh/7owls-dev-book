# Готовим каркас проекта

## Создаем проект Django

Итак, ранее мы уже установили Django. Теперь сделаем базовый каркас проекта.

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ django-admin.py startproject sowls3

Теперь попробуем зайти в новосозданную папку `sowls3` и запустить проект на исполнение:

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ cd sowls3
    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3/sowls3$ python manage.py runserver
    Validating models...
    
    0 errors found
    November 13, 2013 - 10:01:41
    Django version 1.6, using settings 'sowls3.settings'
    Starting development server at http://127.0.0.1:8000/
    Quit the server with CONTROL-C.

Проект успешно запустился, теперь можем открыть браузер и перейти по адресу `http://127.0.0.1:8000/`. Результатом будет следующее:

![](https://dl.dropboxusercontent.com/u/381385/7owls_dev_book/img-03/img001.png)

Кстати, сразу рекомендую выставить для скрипта `manage.py` права на запуск, это позволит немного сократить строку запуска, а пользоваться данным придется часто:

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3/sowls3$ chmod +x manage.py

Теперь можно не дописывать явно `python` при запуске проекта:

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3/sowls3$ ./manage.py runserver

## Используем Git

Поскольку мы уже создали несколько файлов, то пришло самое время начать использовать систему контроля версий. Мы будем использовать Git, это самая популярная на сегодняшний день такая система.

Для хостинга исходников проекта, будем использовать Bitbucket. Создадим там новый проект:

![](https://dl.dropboxusercontent.com/u/381385/7owls_dev_book/img-03/img002.png)

Теперь инициализируем наш репозиторий и подключаем удаленный:

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ git init
    Initialized empty Git repository in /home/korkholeh/Projects/sowls3/.git/
    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ git remote add origin ssh://git@bitbucket.org/korkholeh/7owls-rethinked.git

Теперь нам надо было бы добавить файлы нашего проекта в репозиторий, но просто так сразу это делать нельзя. Сначала нам нужно указать, какие файлы не должны попасть в репозиторий. Есть различные причины не добавлять часть файлов для управления с помощью системы контроля версий. В первую очередь, нет смысла добавлять бинарные файлы (если только это например не картинки или прочая статика). Нет никакого смысла управлять изменениями версий `.pyc` файлов (компилированные в байт-код модули). Python 3.3 складывает такие файлы по подпапкам `__pycache__`, которые необходимо также исключить. Также, нет смысла включать в репозиторий папку виртуального окружения. Чтобы сделать это, создадим в папке проекта файл `.gitignore` и запишем в него следующее:

    env_sowls
    *.pyc
    __pycache__

Теперь добавим файлы в систему контроля версий:

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ git add -A
    
И сделаем коммит:

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ git commit -m "a project skeleton"

После чего можем залить обновления из локального репозитория в удаленный:

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ git push origin master
    
Теперь файлы можно просмотреть на битбакете.

![](https://dl.dropboxusercontent.com/u/381385/7owls_dev_book/img-03/img003.png)