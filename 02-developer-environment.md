# Подготовка рабочего окружения

## Устанавливаем Python 3

Мы будем использовать в работе Python 3.3. Хотя большинство все еще активно использует ветку 2.x, мы попробуем сработать на перспективу.

Вообще, поскольку мы используем Ubuntu 13.10, то Python 3.3 в системе уже установлен. Но, если по какой-то причине это не так, то можно легко его установить следующей командой:

    korkholeh@korkholeh-ThinkPad-T430:~$ sudo apt-get install python3
    
## Готовим виртуальное окружение

Первым делом нам необходим каталог в котором мы и будем вести разработку. Удобно в домашней папке иметь папку Projects для всех проектов. Создадим там новую папку для нашего проекта и зайдем в нее.

    korkholeh@korkholeh-ThinkPad-T430:~/Projects$ mkdir sowls3
    korkholeh@korkholeh-ThinkPad-T430:~/Projects$ cd sowls3
    
Теперь нам необходимо создать новое виртуальное окружение. Это необходимо для того, чтобы не устанавливать все необходимые для работы с нашим проектом библиотеки глобально. Ведь мы не планируем останавливатья лишь на одном проекте, а разным проектам требуются разные библиотеки. Виртуальные окружения - хороший инструмент, позволяющий исключить возможные конфликты и облегчающий разработку. В Python 3.3 (в отличии от более ранних версий) поддержка виртуальных окружений включена в стандартную поставку.

Создаем новое виртуальное окружение, с названием `env_sowls` в папке проекта:

    korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ python3 -m venv env_sowls
    
После этого, в текущей папке появится папка `env_sowls`, со всеми необходимыми для работы интерпретатора файлами. Далее, нам необходимо ее активировать:

    korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ source env_sowls/bin/activate
    
О том, что виртуальное окружение активировано, сообщает его имя, появившееся в скобках, перед приглашением командной строки:

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$

Можем использовать команду `tree` чтобы посмотреть, что находится в этой папке:

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ tree env_sowls
    env_sowls
    ├── bin
    │   ├── activate
    │   ├── python -> python3
    │   └── python3 -> /usr/bin/python3
    ├── include
    ├── lib
    │   └── python3.3
    │       └── site-packages
    └── pyvenv.cfg

Обратите внимание, что папка `lib/python3.3/site-packages` пуста. Это означает, что в нашем виртуальном окружении пока еще не установлена ни одна библиотека. Еще один важный момент - `python` ссылается на `python3`. Т.е., если обычно, при наборе команды `python`, в Ubuntu 13.10 запускается Python 2.7, то при активированном виртуальном окружении, напротив - Python 3.3.

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ python
    Python 3.3.2+ (default, Oct  9 2013, 14:50:09) 
    [GCC 4.8.1] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> 
    
Собственно, это нам и нужно.

## Установка необходимых библиотек

Конечно же, для того чтобы нам начать делать что-то осязаемое, нам потребуется установка довольно большого набора сторонних библиотек. Конечно же не сразу, мы их будем доустанавливать в процессе работы, по мере необходимости.

Любую библиотеку для Python можно установить довольно просто, всего лишь скачав архив, распаковав его и запустив `python setup.py install`. Но, многие библиотеки связаны между собой зависимостями. Кроме того, искать и скачивать дистрибутивы библиотек - крайне утомительное занятие. Поэтому, первое, что мы сделаем - это установим в наше виртуальное окружение `pip`. Это пакетный менджер, который здорово поможет нам в работе.

Сначала, нам понадобится установить `setuptools`. Этот компонент необходим для работы `pip`. О том как его устанавливать, написано на [странице проекта](https://pypi.python.org/pypi/setuptools#installation-instructions).

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ wget https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py -O - | python

Теперь можно устанавиливать `pip`. На официальном сайте [описано как это делать](http://www.pip-installer.org/en/latest/installing.html). Там же мы сможем найти адрес скрипта установки. Скачаем его и запустим:

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ wget https://raw.github.com/pypa/pip/master/contrib/get-pip.py
    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ python get-pip.py

После установки, деактивируем виртуальное окружение, а потом вновь активируем.

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ deactivate 
    korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ source env_sowls/bin/activate
    
Готово. Теперь можно попробовать установить Django:

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ pip install django

Можем теперь проверить успешность установки:

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ python
    Python 3.3.2+ (default, Oct  9 2013, 14:50:09) 
    [GCC 4.8.1] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> import django
    >>> django.VERSION
    (1, 6, 0, 'final', 0)
    >>> 
    
На этом пока закончим. Дополнительные библиотеки будем устанавливать по мере надобности.

Также, мы скачивали два файла в папку проекта, можем их теперь удалить:

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ rm setuptools-1.3.2.tar.gz get-pip.py
    
## Сохранение списка установленных библиотек

Очень важно, после установки новой библиотеки, сохранять список установленных библиотек в отдельный файл. Это нам сильно поможет в будущем, поскольку при разворачивании проекта в новом месте, установку всех необходимых библиотек можно будет произвести одной командой. Создадим файл `requirements.txt` и запишем в него список библиотек.

    (env_sowls) korkholeh@korkholeh-ThinkPad-T430:~/Projects/sowls3$ pip freeze > requirements.txt
    
Теперь у нас будет файл, хранящий в себе информацию об установленных библиотеках и их версиях. Его содержание будет следующим:

    Django==1.6
    
Вот так все просто.

